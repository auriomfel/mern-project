import React from 'react'
import PropTypes from 'prop-types'

const ProfileAbout = ({ profile: {
    bio, company, location, website, skills, user: { name }
} }) => {
    return (
        <div className="profile-about bg-light p-2">
            <h2 className="text-primary">{name.trim().split(' ')[0]}'s Bio</h2>
            <p>
                {bio}
            </p>
            <div className="line"></div>
            <h2 className="text-primary">Skill Set</h2>
            {skills ? (
                <div className="skills">
                    {skills.map((skill, index) => (
                        <div key={index} className="p-1">
                            <i className="fa fa-check"></i> {skill}
                        </div>))
                }
                </div>) : (<span>No Skills found</span>)
            }
        </div>
    )
}

ProfileAbout.propTypes = {
    profile: PropTypes.object.isRequired,
}

export default ProfileAbout
