import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addPost } from '../../actions/post';
/* import { Link } from 'react-router-dom'; */

const PostForm = ({ addPost }) => {
    const [text, setText] = useState('');
    return (

        /* (profile && loading==false) ?
            ( */
                <div className="post-form">
                    <div className="bg-primary p">
                        <h3>Say Something...</h3>
                    </div>
                    <form className="form my-1" onSubmit={e => {
                        e.preventDefault();
                        addPost({ text });
                        setText('')
                    }}>
                        <textarea
                            name="text"
                            cols="30"
                            rows="5"
                            placeholder="Create a post"
                            value={text}
                            onChange={e => setText(e.target.value)}
                            required
                        ></textarea>
                        <input type="submit" className="btn btn-dark my-1" value="Submit" />
                    </form>
                </div>/* ) : (
                <div className="post-form">
                    <h1>You must create a profile first</h1>
                    <Link to='/edit-profile/'>Return</Link>
                </div>
            ) */
    )
}

PostForm.propTypes = {
    addPost: PropTypes.func.isRequired,
    /* loading: PropTypes.bool.isRequired, */
}

/* const mapStateToProps = state => ({
    profile: state.profile.profile,
    loading: state.auth.loading
}) */

export default connect(/* mapStateToProps */null, { addPost })(PostForm)
